import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';

const list = ["Rose","Flower","Orange","Apple","Banana","Nomi","Water","Lemon","Water","Water","Strabery","Flower"];
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StickyHeader(
      header: Container(
        width: double.maxFinite,
        height: 250.0,
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        alignment: Alignment.centerLeft,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisSize: MainAxisSize.max,
            // mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(height: 200.0, color: Colors.grey),
              Text('Free 20IQD discount',
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 26.0,
                  shadows: <Shadow>[
                    Shadow(offset: Offset(6.0, 6.0),blurRadius: 3.0,color: Color.fromARGB(255, 0, 0, 0)),
                    Shadow(offset: Offset(10.0, 10.0),blurRadius: 8.0,color: Color.fromARGB(125, 0, 0, 255))
                  ]
                )
              ),
            ]
          )
        ),
        content: Container(
            padding: EdgeInsets.fromLTRB(0,250.0, 0, 0),
            child: GridView.count(
              crossAxisCount: 4,
              children: items(),
            )
        ),
    );
  }

  items() {
    List<Widget> itemsWidg = List<Widget>();
    for (int i = 0; i < list.length; i++) {
      itemsWidg.add(Container(
        // color: Colors.white,
        padding: EdgeInsets.only(top:8,bottom:2,left:8,right: 8),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          //color: const Color(0xFF66BB6A),
          boxShadow: [BoxShadow(color: Colors.grey[200],blurRadius: 5.0)]
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(
              width: 0.5,
              color: Colors.grey[300],
              style: BorderStyle.solid
            ),
        ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                width: double.maxFinite,
                alignment: Alignment.center,
                color: Colors.white,
                child: Container(
                  height: 20,
                  child: Text(list[i],  style:TextStyle(color:Colors.black, fontSize: 12))
                )
              ),
            ]
          )
        ),
      ));
    }

    return itemsWidg;
  }

}