import 'package:flutter/material.dart';
import 'package:POC/views/bonus.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // Let the ListView know how many items it needs to build.
      itemCount: items.length,
      // Provide a builder function. This is where the magic happens.
      // Convert each item into a widget based on the type of item it is.
      itemBuilder: (context, index) {
        final item = items[index];

        return ListTile(
          title: item.buildTitle(context),
          trailing:item.buildArrow(context),
          leading: item.buildIcon(context),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Bonus(),
              ),
            );
          },

        );
      },
    );
  }
}

abstract class Item {
  Widget buildTitle(BuildContext context);
  Widget buildIcon(BuildContext context);
  Widget buildArrow(BuildContext context);
}

class HeadingItem implements Item {
  final String heading;

  HeadingItem(this.heading);

  Widget buildTitle(BuildContext context) {
    return Text(
      heading,
      style: TextStyle(color: Colors.grey[800]),
    );
  }

  Widget buildIcon(BuildContext context) => null;
  Widget buildArrow(BuildContext context) => null;
}


class ProfileItem implements Item {
  final IconData icon ;
  final String body ;

  ProfileItem(this.icon, this.body);


  Widget buildTitle(BuildContext context) => Text('$body');
  Widget buildIcon(BuildContext context) => Icon(icon);
  Widget buildArrow(BuildContext context) => Icon(Icons.arrow_right);
}

final items = [
  HeadingItem("Personal info"),
  ProfileItem(Icons.verified_user, "Jon Snow"),
  ProfileItem(Icons.call, "0777779944"),
  HeadingItem("Details"),
  ProfileItem(Icons.add_location_sharp, "Addresses"),
  ProfileItem(Icons.favorite, "Favorite Products"),
  ProfileItem(Icons.reorder, "Previous Orders"),
  ProfileItem(Icons.card_giftcard, "Bonus"),
  ProfileItem(Icons.payment, "Payment Options"),
  ProfileItem(Icons.lock, "Change Passwords"),
  ProfileItem(Icons.help, "Help"),
  ProfileItem(Icons.logout, "Logout")
];