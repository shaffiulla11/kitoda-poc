import 'package:flutter/material.dart';
import 'package:POC/views/home.dart';
import 'package:POC/views/profile.dart';

class TabControl extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.green,
            title: Text("Vega"),
          ),
          bottomNavigationBar: menu(),
          body: TabBarView(
            children: [
              Container(child: Home()),
              Container(child: Icon(Icons.search)),
              Container(child: Profile()),
              Container(child: Icon(Icons.card_giftcard)),
            ],
          ),
        ),
      ),
    );
  }

  Widget menu() {
    return Container(
      color: Colors.green,
      child: TabBar(
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white70,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorPadding: EdgeInsets.all(5.0),
        tabs: [
          Tab(
            text: "Home",
            icon: Icon(Icons.house),
          ),
          Tab(
            text: "Search",
            icon: Icon(Icons.search),
          ),
          Tab(
            text: "Profile",
            icon: Icon(Icons.account_circle),
          ),
          Tab(
            text: "Gifts",
            icon: new Stack(
              children: <Widget>[
                new Icon(Icons.card_giftcard),
                new Positioned(
                  right: 0,
                  child: new Container(
                    padding: EdgeInsets.all(1),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: new Text(
                      '25',
                      style: new TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                )
              ],
            ),
          ),
        ],
      ),
    );
  } 
}

