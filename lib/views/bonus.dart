import 'package:flutter/material.dart';

class Bonus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
      ),
      body: Container(
        color: Colors.grey[200],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 150,
              width: double.maxFinite,
              color: Colors.white,
              margin: EdgeInsets.only(bottom:10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children:<Widget>[
                  Text("Your Total Bonus is",  style:TextStyle(color:Colors.grey, fontSize: 22)),
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    padding: EdgeInsets.only(top: 12,bottom:12, right: 40, left: 40),
                    decoration: BoxDecoration(color: Colors.green[100], borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Text("323,412 Point",  style:TextStyle(color:Colors.green, fontSize: 26)),
                  )
                ]
              )
            ),
            Text("   Bonus increases by purchasing more items",  style:TextStyle(color:Colors.grey, fontSize: 13))
          ]
        )
      )
    );
  }
}

// Container(
//             height: 120,

//           decoration: BoxDecoration(
//             color: Colors.grey,
//             borderRadius: BorderRadius.all(Radius.circular(5)),
//             border: Border.all(
//               width: 0.5,
//               color: Colors.grey[300],
//               style: BorderStyle.solid
//             ),
//         ),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisSize: MainAxisSize.max,
//             mainAxisAlignment: MainAxisAlignment.end,
//             children: <Widget>[
//               Container(
//                 width: double.maxFinite,
//                 alignment: Alignment.center,
//                 color: Colors.white,
//                 child: Container(
//                   height: 20,
//                   child: Text("12",  style:TextStyle(color:Colors.black, fontSize: 12))
//                 )
//               ),
//             ]
//           )
//         ),
//     ),
